Profile
=======

Currently working at [Orange] as Front-end developper.

##Technologies

###Web :
- HTML5
- CSS3
- Javascript
- AngularJS
- ReactJS & Flux (Currently learning)
- Highcharts
- jQuery

###Other :
- Grunt
- Gulp
- Yeoman
- Apache
- Nginx
- Docker
- Jenkins

##Contact

* Mail : david.jomain at gmail.com
* LinkedIn : https://www.linkedin.com/in/davidjomain
* Twitter : https://twitter.com/Kaynshin/